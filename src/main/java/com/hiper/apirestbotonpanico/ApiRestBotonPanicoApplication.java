package com.hiper.apirestbotonpanico;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiRestBotonPanicoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiRestBotonPanicoApplication.class, args);
	}

}
