package com.hiper.apirestbotonpanico.controllers;

import java.util.List;
import java.util.function.Supplier;

import javax.management.relation.RelationTypeNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.hiper.apirestbotonpanico.dto.CategoriaDTO;
import com.hiper.apirestbotonpanico.mappers.CategoriaMapper;
import com.hiper.apirestbotonpanico.model.Categoria;
import com.hiper.apirestbotonpanico.repositories.CategoriaRepository;

@RestController
@RequestMapping("/categorias")
public class CategoriaController {

    private final CategoriaRepository categoriaRepository;

    public CategoriaController(CategoriaRepository categoriaRepository) {
        this.categoriaRepository = categoriaRepository;
    }

    @Autowired
    private CategoriaMapper categoriaMapper;
    
    @GetMapping
    public List<CategoriaDTO> getAll() {
        List<Categoria> categorias = categoriaRepository.findAll();
        return categoriaMapper.toDTOList(categorias);
    }

    @PostMapping
    public Categoria create(@RequestBody Categoria categoria) {
        return categoriaRepository.save(categoria);
    }

    @PutMapping("/{id}")
    public Categoria update(@PathVariable Short id, @RequestBody Categoria categoria) {
        Categoria existingCategoria = new Categoria();
        // Categoria existingCategoria = categoriaRepository.findById(id).orElseThrow((Supplier<? extends X>) () -> new ResourceNotFoundException("Categoria", "id", id));
        // Update the fields of existingCategoria based on categoria
        // ...
        return categoriaRepository.save(existingCategoria);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Short id) {
        if (!categoriaRepository.existsById(id)) {
            //throw new RelationTypeNotFoundException("Categoria", "id", id);
        }
        categoriaRepository.deleteById(id);
    }
}
