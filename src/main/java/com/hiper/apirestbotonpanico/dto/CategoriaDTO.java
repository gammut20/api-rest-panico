package com.hiper.apirestbotonpanico.dto;

public class CategoriaDTO {

    private Short id;
    private String descripcion;
    private char estado;

    // getter y setter

    public Short getId() {
        return id;
    }

    public void setId(Short id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public char getEstado() {
        return estado;
    }

    public void setEstado(char estado) {
        this.estado = estado;
    }
}
