package com.hiper.apirestbotonpanico.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hiper.apirestbotonpanico.model.Categoria;

public interface CategoriaRepository extends JpaRepository<Categoria, Short> {
}